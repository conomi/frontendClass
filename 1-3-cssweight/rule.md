行内样式 > id 选择器 > 类选择器/属性选择器/伪类 > 元素选择器/伪元素

选择器/权重
a/0,0,0,1

a p/0,0,0,2

a p > span/0,0,0,3

.test/0,0,1,0

a.test/0,0,1,1

.test.card/0,0,2,0

\#king/0,1,0,0

a\#king.orange/0,1,1,1

a\#king.orange.card/0,1,2,1

style="xxxx"/1,0,0,0
