​​git init 创建仓库

git status 查看文件状态

git add ./xxx 将./xxx路径下的文件提交到暂存区

git rm (--cached) xxx 将xxx从目录删除并从管理树删除 (仅从管理树删除)

git log 查看git log

git config user.name/user.email (--global) "xxx" 设置用户名邮箱

git commit -m "xxx" 提交描述为xxx的commit

git push 推送更新到远程仓库

git push --set-upstream origin xxx 把本地的xx分支同步到远程仓库

git fetch/merge (pull) 下载/合并最新内容

git merge xxx 合并xxx分支到当前分支

git checkout (-b) xxx 切换到xxxcommit或分支（-b创建分支）

git branch 查看所有分支

git reset xxxxxxxx (--hard) 重置到Xxx分支

git remote set-url origin URL 切换远程仓库地址到URL

git clone xxx 下载 xxx 仓库

git diff xxx xxx (> xxx.patch） 比较文件差异 (生成补丁)​​​​