只整理了一些常用的，有遗漏以后再补

# 基础快捷键 (windows下)

ctrl + x 剪切

ctrl + c 复制 （不选中任何东西复制光标所在行）

ctrl + z 撤销

ctrl + y 重做

ctrl + S 保存 （加shift另存为）

ctrl + [ 或 ] 向前或向后缩进

alt + ↑ 或 ↓ 将光标所在行上移或下移

Home 光标移动到行头

End 光标移动到行尾
（上述行为 + ctrl 键移动到页头或页尾）

ctrl + / （当前行或选中区域注释/取消注释）

Tab 缩进

shift + alt + F 格式化代码

# 导航快捷键

F3 选中项查询下一个

ctrl + D 匹配所有和选中区域一样的部分并多行选中下一个相同段落

ctrl + F 查询

ctrl + H 替换

alt + ← 或 → 向上/向下翻页

F8 跳转到下一个错误警告 （加shift跳转上一个）

alt + enter 选中当前查询中的所有项目（需要ztrl + f 打开查询状态）

# 多行光标

alt + 鼠标左键 在鼠标点击处添加光标

ctrl + alt + ↑ 或 ↓ 上下插入光标

ctrl + shift + L 直接选中所有与选中区域相同的段落

# 终端快捷键

ctrl + ` 打开集成终端 （个人改成了alt + +号 见仁见智可以自己改一个顺手的）

ctrl + Ins 复制

shift + Ins 粘贴
